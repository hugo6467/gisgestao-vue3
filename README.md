# Prova de conceito p/ GisGestão usando VUE 3.0 com compositions API

_POC que testa novas tecnologias como o Vitte para o bundle e VUE-3 usando compositions-api  
Tem o foco de garantir a melhor peformance possível para o front-end, utiliza-se de técnicas e novas tecnologias para chegar a este resultado_

**Para rodar o projeto**
> yarn install 

> yarn dev


**Para gerar e rodar a build**
> yarn build  

> serve -s dist




