import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/packages/theme-chalk/src/index.scss'
import store from '@/store'
import staticConfig from '@/staticConfig'

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.provide('staticConfig', staticConfig.config)

const getAppConfiguration = () => {
  try {
    axios.get(`http://${window.location.host}/config.json`).then(result => {
      app.provide('configuration', result)
      app.provide('store', store)
      app.mount('#app')
    })
  } catch (error) {
    axios.get('@/../public/config.json').then(result => {
      app.provide('configuration', result)
      app.provide('store', store)
      app.mount('#app')
    })
  }
}
getAppConfiguration()
