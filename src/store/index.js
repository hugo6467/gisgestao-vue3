/**
 * Store da aplicação.
 * Uso = import store '@/store'
 * store.methods.set(loggedUser, XPTO)
 */

import { reactive, readonly } from 'vue'

const state = reactive({
  loggedUser: 0,
  mapInstances: [],
  graphicValue: null
})

const staticState = {
  mapInstances: [],
  leafletVueComponents: [],
  leafletInstance: null
}

const methods = {
  /**
   * Método para setar propriedades no state estático (não reativo)
   * @param {String} stateName - nome da propriedade a ser setada
   * @param {*} value - valor a ser setado
   * @param {Boolean} replace - Se deve ou não substituir o valor inteiro caso seja um array
   */
  setStatic (stateName, value, replace = false) {
    if (Array.isArray(staticState[stateName]) && !replace) {
      staticState[stateName].push(value)
    } else {
      staticState[stateName] = value
    }
  },

  /**
   * Retorna valor de uma propriedade no state estático
   * @param {String} name - nome da propriedade
   * @returns {*} O valor do state
   */
  getStatic (name) {
    return staticState[name]
  },

  /**
   * Método para setar propriedades no state reativo
   * @param {String} stateName - nome da propriedade a ser setada
   * @param {*} value - valor a ser setado
   * @param {Boolean} replace - Se deve ou não substituir o valor inteiro caso seja um array
   */
  set (stateName, value, replace) {
    if (Array.isArray(state[stateName]) && !replace) {
      state[stateName].push(value)
    } else {
      state[stateName] = value
    }
  },

  /**
   * Retorna valor de uma propriedade no state reativo
   * @param {String} name - nome da propriedade
   * @returns {*} O valor do state
   */
  get (name) {
    return state[name]
  },

  /**
   * Método responsável por construir instâncias de mapas arcgis
   * @param {Number} numberOfMaps - o número de instâncias a serem construídas
   * @param {Function} callback - Função de callback que é chamada a cada instância construída
   */
  buildMaps (numberOfMaps, callback) {
    let i = 0
    import('@arcgis/core/Map').then(resultMap => {
      import('@arcgis/core/views/MapView').then(async resultMapView => {
        const Map = resultMap.default
        const MapView = resultMapView.default
        while (i < numberOfMaps) {
          const arcgisMap = new MapView({
            container: 'container',
            id: i,
            map: new Map({
              basemap: 'topo-vector'
            }),
            zoom: 5,
            center: [-47.779541015625, -15.780360118526076],
            popup: {
              autoOpenEnabled: false
            }
          })

          /* const resultFeatureLayer = await import('@arcgis/core/layers/FeatureLayer')
          const FeatureLayer = resultFeatureLayer.default
          const pontosFiscalizacao = new FeatureLayer({
            url: 'https://gisserver.gisgestao.com.br/server/rest/services/GisColetor/FeatureServer/0'
          })
          arcgisMap.map.add(pontosFiscalizacao)

          const trechoDeFiscalizacao = new FeatureLayer({
            url: 'https://gisserver.gisgestao.com.br/server/rest/services/GisColetor/FeatureServer/1'
          })
          arcgisMap.map.add(trechoDeFiscalizacao)

          const areaFiscalizacao = new FeatureLayer({
            url: 'https://gisserver.gisgestao.com.br/server/rest/services/GisColetor/FeatureServer/2'
          })
          arcgisMap.map.add(areaFiscalizacao)

          const estados = new FeatureLayer({
            url: 'https://gisserver.gisgestao.com.br/server/rest/services/GisColetor/FeatureServer/3'
          })
          arcgisMap.map.add(estados)

          const municipios = new FeatureLayer({
            url: 'https://gisserver.gisgestao.com.br/server/rest/services/GisColetor/FeatureServer/4'
          })
          arcgisMap.map.add(municipios) */
          callback(arcgisMap, i, numberOfMaps)
          i++
        }
      })
    })
  },

  buildLeafletMap (numberOfMaps, cb) {
    let i = 0
    import('leaflet/dist/leaflet-src.esm').then(srcMap => {
      this.setStatic('leafletInstance', srcMap)
    })
    while (i < numberOfMaps) {
      import('@/components/View/Leaflet.vue').then(component => {
        cb(component.default, i, numberOfMaps)
      })
      i++
    }
  }
}
export default {
  state: readonly(state),
  ...methods,
  staticState: readonly(staticState)
}
