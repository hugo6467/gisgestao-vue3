import { createRouter, createWebHistory } from 'vue-router'
const Home = () => import('@/components/Home.vue')
const MapViewer = () => import('@/components/View/MapViewer.vue')
const Detail = () => import('@/components/Detail.vue')
const routerHistory = createWebHistory()

const router = createRouter({
  history: routerHistory,
  routes: [
    {
      name: 'Home',
      path: '/',
      component: Home,
      props: true
    },
    {
      name: 'mapViewer',
      path: '/mapViewer',
      component: MapViewer,
      props: true
    },
    {
      name: 'detail',
      path: '/detail',
      component: Detail
    }
  ]
})
export default router
