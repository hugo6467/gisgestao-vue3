const onClickItem = (clickEvent, labels) => {
  return clickEvent.chart.getElementsAtEventForMode(clickEvent.native, 'nearest', { intersect: true }, true)[0] && clickEvent.chart.getElementsAtEventForMode(clickEvent.native, 'nearest', { intersect: true }, true)[0].element
    ? labels[clickEvent.chart.getElementsAtEventForMode(clickEvent.native, 'nearest', { intersect: true }, true)[0].element.$context.dataIndex]
    : null
}
export default {
  onClickItem
}
