import axios from 'axios'

const postRequest = async (url, params, options) => {
  try {
    return axios.post(url, params, options)
  } catch (err) {
    console.error(`Error na requisição "${err}"`)
    throw new Error(err)
  }
}

const getRequest = async (url, options) => {
  try {
    return axios.get(url, options)
  } catch (err) {
    console.error(err)
  }
}

export default {
  postRequest,
  getRequest
}
